package com.anec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MainWindow extends JFrame {

    private final static String VERSION = "0.1.2";

    private final JLabel labelDiameter = new JLabel("Диаметр(мм): ");
    private final JTextField textFieldDiameter = new JTextField();

    private final JLabel labelCircumference = new JLabel("Длина окружности(мм): ");
    private final JTextField textFieldResult = new JTextField();

    private final JButton buttonCalculate = new JButton("Рассчитать");
    private final JButton buttonAbout = new JButton("О программе");

    public MainWindow() {
        initialize();
    }

    private void initialize() {
        setTitle("Calculating The Circumference");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(475, 128);
        setLocationRelativeTo(null);
        setResizable(false);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "Не удалось применить системный стиль.\n" + e.getMessage(),
                    this.getTitle(), JOptionPane.ERROR_MESSAGE);
        }

        Container container = getContentPane();
        container.setLayout(new GridLayout(3, 2, 5, 5));

        container.add(labelDiameter);

        textFieldDiameter.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttonCalculate.doClick();
                }
            }
        });
        container.add(textFieldDiameter);

        container.add(labelCircumference);

        textFieldResult.setEditable(false);
        container.add(textFieldResult);

        buttonCalculate.addActionListener(e -> {
            if (!textFieldDiameter.getText().isEmpty()) {
                int diameter;

                try {
                    diameter = Integer.parseInt(textFieldDiameter.getText().replace(',', '.'));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Диаметр введён неправильно", getTitle(),
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                textFieldResult.setText(String.valueOf(CalculatorCircumference.Calculate(diameter)));
            } else {
                JOptionPane.showMessageDialog(null, "Диаметр не введён", getTitle(),
                        JOptionPane.ERROR_MESSAGE);
            }
        });
        container.add(buttonCalculate);

        buttonAbout.addActionListener(e -> JOptionPane.showMessageDialog(null,
                "Эта программа предназначена для расчёта\n" +
                        "                          длины окружности.\n\n" +
                        "                                 Версия: " + VERSION + "\n\n" +
                        "                               Created by Anec\n",
                "О программе", JOptionPane.PLAIN_MESSAGE));
        container.add(buttonAbout);

        for (Component c : this.getComponents()) {
            SwingUtilities.updateComponentTreeUI(c);
        }
    }
}
