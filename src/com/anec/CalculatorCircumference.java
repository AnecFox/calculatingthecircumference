package com.anec;

public class CalculatorCircumference {

    private final static double PI = 3.14;

    public static int Calculate(int diameter) {
        return (int) (diameter * PI);
    }
}
